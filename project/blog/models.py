from django.db import models
from django.contrib.auth.models import User
from django.urls import reverse
from datetime import datetime

class Article(models.Model):
    title = models.CharField(max_length=50)
    slug = models.SlugField(null=False, unique=True)
    content = models.TextField()
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    publicationDateTime = models.DateTimeField(default=datetime.now, blank=True)
    switch = models.BooleanField(default=True)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('article_detail', kwargs={'pk': self.id, 'slug': self.slug})
        
class ContactRequest(models.Model):
    email = models.EmailField()
    name = models.CharField(max_length=255)
    content = models.TextField()
    date = models.DateField()

    def __str__(self):
        return self.name
