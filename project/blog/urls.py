from django.urls import path
from blog.views import ArticleListView, ArticleDetailView
from blog import views

urlpatterns = [
    path('<int:pk>/<slug:slug>', ArticleDetailView.as_view(), name='article_detail'),
    path('', ArticleListView.as_view(), name='article_list'),
    path('contact/', views.contact_view, name='contact'),
]
