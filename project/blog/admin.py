from django.contrib import admin
from blog.models import Article, ContactRequest

class ArticleAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("title",)}

class ContactRequestAdmin(admin.ModelAdmin):
    def has_change_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return True

admin.site.register(Article, ArticleAdmin)
admin.site.register(ContactRequest, ContactRequestAdmin)