from django.shortcuts import render
from blog.models import Article
from django.views.generic import ListView, DetailView
from blog.forms import ContactForm
from django.core.mail import send_mail, BadHeaderError, send_mass_mail
from django.shortcuts import redirect
from django.conf import settings


class ArticleListView(ListView):
   model = Article
   paginate_by = 5
   template_name = 'article_list.html'


class ArticleDetailView(DetailView):
   model = Article
   template_name = 'article_detail.html'

def contact_view(request):
   if request.method == 'POST':
      form = ContactForm(request.POST)
      if form.is_valid():
         form.save()
         email = form.cleaned_data['email']
         content = form.cleaned_data['content']
         name = form.cleaned_data['name']
         send_mail(
            'Reply-to',
            'Content: ' + content + ', Email: ' + email + ', Name: ' + name,
            email,
            ['debug@mir.de', 'dimash.junusov@gmail.com'],
            fail_silently=False,
         )
         return render(request, 'success.html')
   
   form = ContactForm()
   context = {'form': form}
   return render(request, 'contact.html', context)
