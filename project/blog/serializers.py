
from dataclasses import field
from blog.models import Article, ContactRequest
from rest_framework import serializers

class ArticleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Article
        fields = ['id', 'title', 'slug', 'content', 'author', 'publicationDateTime', 'switch']

class ContactRequestSerializer(serializers.ModelSerializer):
    class Meta:
        model = ContactRequest
        fields = ['email', 'name', 'content', 'date']
