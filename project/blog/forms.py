from django.forms import ModelForm
from blog.models import ContactRequest
from django import forms

# class ContactForm(forms.Form):
#     email = forms.EmailField()
#     name = forms.CharField(max_length=32)
#     content = forms.CharField()
#     date = forms.DateField()

class ContactForm(ModelForm):
    class Meta:
        model = ContactRequest
        fields = '__all__'

